﻿using AnHo_Counter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnHo_CounterTest.TestDbSet
{
    public class TestCounterDbSet : TestDbSet<Counter>
    {
        public override Counter Find(params object[] keyValues)
        {
            return this.FirstOrDefault();
        }
    }
}
