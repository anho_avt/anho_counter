﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnHo_CounterTest.TestContext;
using AnHo_Counter.Controllers;
using System.Web.Mvc;
using AnHo_Counter.Models;

namespace AnHo_CounterTest
{
    [TestClass]
    public class TestHomeController
    {
        [TestMethod]
        public void TestIncreaseCounter()
        {
            var testContext = new TestCounterContext();
            var controller = new HomeController(testContext);

            // get counter            
            var oldCounter = (testContext.Counters.Find() ?? new Counter()).Count;
            var result = controller.Count() as RedirectResult;
            var testCounter = testContext.Counters.Find();

            // compare action result
            Assert.AreEqual(result.Url, "Index");
            // compare count result
            if (oldCounter < 10)
            {
                Assert.AreEqual(testCounter.Count, oldCounter + 1);
            }
            else
            {
                Assert.AreEqual(testCounter.Count, oldCounter);
            }
        }

        [TestMethod]
        public void TestIncreaseCounter_TwoTime()
        {
            var testContext = new TestCounterContext();
            var controller = new HomeController(testContext);

            // get counter
            // first time            
            var oldCounter = (testContext.Counters.Find() ?? new Counter()).Count;
            var result = controller.Count() as RedirectResult;

            // second time
            oldCounter = (testContext.Counters.Find() ?? new Counter()).Count;
            result = controller.Count() as RedirectResult;

            var testCounter = testContext.Counters.Find();

            // compare action result
            Assert.AreEqual(result.Url, "Index");
            // compare count result
            if (oldCounter < 10)
            {
                Assert.AreEqual(testCounter.Count, oldCounter + 1);
            }
            else
            {
                Assert.AreEqual(testCounter.Count, oldCounter);
            }
        }

        [TestMethod]
        public void TestResetCounter()
        {
            var testContext = new TestCounterContext();
            var controller = new HomeController(testContext);
            
            // first time            
            var oldCounter = (testContext.Counters.Find() ?? new Counter()).Count;
            var result = controller.Count() as RedirectResult;

            // reset
            result = controller.Reset() as RedirectResult;
            var testCounter = testContext.Counters.Find();

            // compare action result
            Assert.AreEqual(result.Url, "Index");
            // compare count result
             Assert.AreEqual(testCounter.Count, 0);
        }
    }
}
