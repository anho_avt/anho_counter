﻿using AnHo_Counter.Context;
using AnHo_Counter.Models;
using AnHo_CounterTest.TestDbSet;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnHo_CounterTest.TestContext
{
    public class TestCounterContext : ICounterContext
    {
        public TestCounterContext()
        {
            this.Counters = new TestCounterDbSet();
        }

        public DbSet<Counter> Counters { get; set; }

        public int SaveChanges()
        {
            return 0;
        }

        public void Dispose() { }
    }
}
