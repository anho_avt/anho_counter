﻿using AnHo_Counter.Context;
using AnHo_Counter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AnHo_Counter.Controllers
{
    public class HomeController : Controller
    {
        private ICounterContext db = new CounterContext();

        public HomeController() { }

        public HomeController(ICounterContext context)
        {
            db = context;
        }

        public ActionResult Index()
        {
            Counter counter = db.Counters.FirstOrDefault() ?? new Counter();
            return View(counter);
        }

        public ActionResult Count()
        {
            Counter counter = db.Counters.FirstOrDefault() ?? db.Counters.Add(new Counter() { Count = 0, CreatedDate = DateTime.UtcNow, ModifiedDate = DateTime.UtcNow });
            if (counter.Count < 10)
            {
                counter.Count++;
                counter.ModifiedDate = DateTime.UtcNow;
                db.SaveChanges();
            }
            return Redirect("Index");
        }
        public ActionResult Reset()
        {
            Counter counter = db.Counters.FirstOrDefault();
            if (counter != null)
            {
                counter.Count = 0;
                counter.ModifiedDate = DateTime.UtcNow;
                db.SaveChanges();
            }
            return Redirect("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}