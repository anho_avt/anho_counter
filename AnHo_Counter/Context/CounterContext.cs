﻿using AnHo_Counter.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AnHo_Counter.Context
{
    public class CounterContext : DbContext, ICounterContext
    {
        public CounterContext() : base("CounterContext")
        {
        }
        public DbSet<Counter> Counters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Counter>()
                .Property(c => c.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}