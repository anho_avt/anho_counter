﻿using AnHo_Counter.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AnHo_Counter.Context
{
    public interface ICounterContext : IDisposable
    {
        DbSet<Counter> Counters { get; }
        int SaveChanges();
    }
}